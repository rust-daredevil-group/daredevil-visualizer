
#import serial
import io
from socket import *
import time
import struct
from time import sleep



# In the Case of using Sensors sending data over Serial, 
# use these with appropriate settings.
#ser_port = '/dev/ttyACM0'
#ser = serial.Serial(kek, 9600)

# 
x = 0
var = 0;
var2 = 0;

from random import randint

client_socket = socket(AF_INET, SOCK_STREAM)

# Creates a connection to the Localhost to send data to
def setup_local():
    print("Connecting to local...")
    server_name, server_port = '127.0.0.1', 7777
    client_socket.connect((server_name, server_port))
    x = 1

# Creates a connection to a remote IP to send data to
def setup_remote():
    print("Connecting to remote...")
    server_name, server_port = '130.240.200.93', 7777
    client_socket.connect((server_name, server_port))
    x = 1

def printyboye():
    var2 = ser.readline().decode("ascii").strip()
    print("NOCONNET:var2:  " + str(var2))


#COMMENT OUT ONE OF THESE OR BOTH
#setup_local()
#setup_remote()

while True: 

    # Randomizes 4 integers to be sent over ethernet later as one pack of 64byte data.
    var1 = str(randint(1, 1000))
    var2 = str(randint(1, 1000))
    var3 = str(randint(1, 1000))
    var4 = str(randint(1, 1000))

    # Prevents Crashing whenever Broken Pipe Errors occur. 
    # And they will, because you're debugging, not pushing production.
    try:
        # Safety check to make sure appropriate data is sent. 
        # Again, this is debugging. If var1 is correct, the programmer should have made var2,3,4 correct.
        # Whatever values they are, either from a sensor or just random generation.
        if int(float(var1)) > 0:
            # Crash-protection against issues when sending data
            try:
                p = struct.pack('HHHH', int(float(var1)), int(float(var2)), int(float(var3)), int(float(var4)))
                client_socket.send(p)
                print("sent: " + str(p))
                #sleep(1)
            except Exception as e:
                print("Error::")
                print(e)
                sleep(2)

                # Tries to reconnect to either local OR remote if anything fails. 
                setup_local()
                #setup_remote()
    except Exception as e:
        print("Another Error::")
        print(e)
        continue

