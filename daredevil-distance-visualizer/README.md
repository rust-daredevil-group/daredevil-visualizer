# Daredevil-Distance-Vizualiser

This program is a CLI for rendering an object's distance relative one or more sensors. 
It both allows for visual renders and printing of raw data values to ensure that 
you can always get a good idea of the object's position.

The software is split into five different major rust .rs files. 

## How to Use

1. Decide how many Sensors you're going to use. Modify the `main.rs` file 
	and create any arbitrary amount of sensors you want to render data from.
	Add these to the `sensors` array and change the constant `SENSOR_AMOUNT`
	to the amount of sensors you'd like to render. Keep in mind,
	you are unlikely able to render more sensors than you have screen space.

2.	Modify the `main.rs` to assign `IP` and `Port` 
	to what you want the program to be listening for TCP connections on.
	`127.0.0.1:XXXX` is a handy ip:port (localhost) and recommended for debugging.
	Any other IP and port is definitely usable as well.

3. `cargo run`

4. Transmit data from something external to your choosen IP:Port address.
	The data should be in an 8 bytes data format. 
	(The 8 bytes should be four shifted u16 values 
	consisting of four different range data (in millimetres) from four different sensors.
	Preferably in order of sensors 1,2,3,4. Ex. [u16, u16, u16, u16] (= 8 bytes)).

5.	Watch it work.

Simplified Instructions:
* Set IP and Port

* Cargo run

* Send 8 bytes (four shifted u16 values for each sensor's range data value (millimeters)) 
	to the IP from external source.

## Files

### main.rs
This file holds the main() function.

A few constans may need to be tweaked by a programmer depending on the amount 
of sensors you wish to use and how quickly the CLI should update.

The main function starts off with defining one or several variables using
the sensor::Sensor struct, like this:

```
let s = sensor::new_sensor(1000, 200, sensor::new_gui(16, 10, 70, PRINT_DELAY));
```

`s` has thus become a sensor::Sensor object. 

The parameters are the following:

```
new_sensor(max_distance, warning_distance, sensor::new_gui(x_position, y_position, h_blocks, delay_of_printing_constant));
```
After this, an IP and Port is defined, an array of pointers to the sensors is created and sent
to a function called do_stuff, which is essentially a function that runts testing and initializes the listening state of the program. 

### gui.rs
This file contains all of the necssary printing-onto-screen CLI code. It uses the 
extern crate `termion` to manipulate colors, clears, cursors, gotos and whatnot.
There are plenty of comments in the code to explain most of the functionality, so use that for reference.
Any CLI-rendering should be contained to this file.

### sensor.rs
Contains a public struct `Sensor`, a public struct `Gui`, and two functions that build new sensors and gui (`new_sensor, new_gui`).

Please check the comments and code of sensor.rs for reference. It's quite self-explanatory in general and the comments should help you figure out the rest. 

### listener.rs
It is here the TCP-connections are listened for and where TCP-stream data is received and handled.
The main functionality is that the function 'listener' listens for any outside connection and data being sent 
to the program, and then handles that data accordingly. Currently, 8 bytes of data is supposed to be receieved and split
into four different u16 values, with each u16 being distance-values (mm) from and in order of sensors 1, 2, 3, 4.

Read the code and comments if you wanna know how it works.

## tests.rs
Here goes all the tests for the program. Currently they are simple tests for the CLI
to make sure that all blocks rendered can be reached and that functionality is working as intended.
There's not much to say about it, but testing the CLI is necessary when tweaking functionality.

Read the code and comments for help.


