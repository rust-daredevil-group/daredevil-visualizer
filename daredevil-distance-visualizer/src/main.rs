#![allow(dead_code)]
#![allow(warnings)]

use std::time::Duration;
use std::thread;
use rand::Rng;
use std::string::ToString;

//TCP
use std::net::{IpAddr, Ipv4Addr};
use std::net::TcpListener;
use std::net::TcpStream;
use std::io::prelude::*;

mod listener;
mod tests;
mod gui;
mod sensor;

use crate::sensor::Sensor;
use tests::*;

/////////////////////////////////////////////////

//Constants
const PRINT_DELAY: u64 = 2;
const BUFFER_SIZE: usize = 20; 
const SENSOR_AMOUNT: usize = 4;

/////////////////////////////////////////////////

fn main() {   
    //Create four new sensor::Sensor objects. 
    let s1 = sensor::new_sensor(1000, 200, sensor::new_gui(15, 10, 70, PRINT_DELAY));
    let s2 = sensor::new_sensor(1000, 200, sensor::new_gui(15, 22, 70, PRINT_DELAY));
    let s3 = sensor::new_sensor(1000, 200, sensor::new_gui(15, 34, 70, PRINT_DELAY));
    let s4 = sensor::new_sensor(1000, 200, sensor::new_gui(15, 46, 70, PRINT_DELAY));

    // Sets up IP and Port for the program to listen on.
    let ip = IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1));
    let port: u16 = 7777;

    // Puts the pointers to the sensors into a pre-defined size array.
    let sensors: [&sensor::Sensor; SENSOR_AMOUNT] = [&s1, &s2, &s3, &s4];

    do_stuff(&sensors, ip, port); // Runs the Program

    println!("Exited program!"); 
}

fn do_stuff(s: &[&sensor::Sensor], ip: IpAddr, port: u16) {
    gui::init_map(&s);                           // Initiates the GUI  
    tests::print_pos_incremental(&(s[0]), 1);    // Runs a test on the CLI
    tests::print_pos_incremental(&(s[1]), 1);    // Runs a test on the CL 
    tests::print_pos_incremental(&(s[2]), 1);    // Runs a test on the CL 
    tests::print_pos_incremental(&(s[3]), 1);    // Runs a test on the CL 
    gui::init_map(&s);                           // Initiates the GUI  

    listener::listener(&s, ip, port); // Puts program into Listening Mode on ip:port 
}
