use rand::Rng;

use crate::gui::print_position;
use crate::sensor::Sensor;


//Lmao have fun with these. I ain't commenting shit. 
//Sorry (nah nah billy, not really) for lying in the README.md 

// Gives randomly generated value x [min < x < max] into the Position Printing.
// For some reason I hard coded it for 30 random numbers. 
// And I don't care to fix it either. It's trivial and I don't use the test regardless.
pub fn print_pos_rng(s: &Sensor) {
    let mut count = 0;
    let mut rng_pos: u16;
    loop{
        rng_pos = rand::thread_rng().gen_range(s.gui.min_blocks, s.gui.max_blocks);
        print_position(s, s.mm_per_block*rng_pos, rng_pos);
        count = count + 1;
        if count == 30 { break; }
    }
}


// Gives randomly generated value x [min <= z < x < y <= max] into the Position Printing
// This one goes back and forth like crazy for 'iterations' iterations. 
// It's fun to look at I guess.
pub fn print_pos_rng_intervals(s: &Sensor, iterations: u16) {
    let mut count = 0;
    let mut pos: u16;;
    let mut inc_dec_mode = 0; //0 inc, 1 dec.
    let mut rng_goto: u16;

    pos = rand::thread_rng().gen_range(1, s.gui.max_blocks+1);
    print_position(s, pos*s.mm_per_block, pos);
    println!("{}:{}", pos, s.gui.max_blocks+1);
    loop {
        count = count + 1;
        if count == iterations { break; }

        if inc_dec_mode == 0 {
            rng_goto = rand::thread_rng().gen_range(pos+1, s.gui.max_blocks+1);
            while pos < rng_goto {
                pos = pos + 1;
                print_position(s, pos*s.mm_per_block, pos);
            }
            inc_dec_mode = 1;
        } else {
            rng_goto = rand::thread_rng().gen_range(1, pos);
            while pos > rng_goto {
                pos = pos - 1;
                print_position(s, pos*s.mm_per_block, pos);
            }
            inc_dec_mode = 0;
        }
    }
}


// Gives continuous input value x [min <= x <= max] for positions to be printed. 
// x goes up > reaches max > then goes down > reaches min > ends.
pub fn print_pos_incremental(s: &Sensor, start_block: u16){
    let mut pos = start_block;
    let mut mode = 0; //0 inc, 1 dec
    loop {
        print_position(s, s.mm_per_block*pos, pos);
        match mode {
            0 => {
                if pos < s.gui.max_blocks { pos = pos + 1; }
                else { mode = 1; pos = pos - 1; }
            }
            1 => {
                if pos > s.gui.min_blocks { pos = pos - 1; }
                else { mode = 0; break;}
            }
            _ => { 
                println!("What the fugg mode should be 1 or 0.");
                panic!();
            }
        }
    }
}

