
// Sensor Struct
pub struct Sensor {
    pub max_distance: u16,    
    pub warning_distance: u16,
    pub gui: Gui,             
    pub mm_per_block: u16,    
}

pub struct Gui {
    pub print_delay: u64,     
    pub x: u16,               
    pub y: u16,                
    pub max_blocks: u16,       
    pub min_blocks: u16,       
}
pub fn new_sensor(max_distance: u16, warning_distance: u16, gui: Gui) -> Sensor {
    Sensor {
        max_distance, 
        warning_distance, 
        mm_per_block: (max_distance/gui.max_blocks),
        gui,
    }
}

pub fn new_gui(x: u16, y: u16, max_blocks: u16, print_delay: u64) -> Gui {
    Gui {
        x,
        y,
        max_blocks,
        min_blocks: 1,
        print_delay,
    }
}

