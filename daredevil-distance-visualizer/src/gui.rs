
// This bad boy allows us to manipulate the CLI similar to ncurses.
// Clear, Color, Goto, you name it. 
extern crate termion;
use termion::{color, clear, cursor, style};
use termion::raw::IntoRawMode;
use std::io::{Write, stdout};
use std::thread;
use std::time::Duration;

#[path = "../src/sensor.rs"]
mod sensor;

// Note, create::sensor::Sensor is the sensor import in main.
use crate::sensor::Sensor;


// Listen up real close. READ TERMION DOCUMENTATION.
// Just play around with termion and I assure you it will feel like reflexes in no time.
// The code looks convoluted but makes absolutely so much sense. 
// READ TERMION DOCUMENTATION
//
// I will refer to a 'distance-block' as an '=' in the CLI.


// This is the function that controls all other functions. 
// Anything sent into this will kickstart the drawing of the whole CLI for any sensor.
pub fn print_position(s: &Sensor, current_distance: u16, pos: u16) {
    let mut stdout = stdout().into_raw_mode().unwrap();

    print_map(s);               // Prints the 'distance blocks'
    print_x(s, pos);            // Prints an X on the distance-block most appropriate
    print_x_history(s, pos);    // Prints where X has been at (history)
    print_block_num(s, pos);    // Prints the number of the block X is at.
    print_pos_distances(s, current_distance, pos); // Prints the distance data in text.
    print_end_clear(s);         // Moves the cursor out of the CLI just to look good.

    stdout.flush();             // Forces print onto the CLI

    print_delay(s.gui.print_delay); // Delays how quick the GUI for each sensor updates.
}

// Initializes 'the map', basically the CLI gui that data will be printed onto.
// It's made to look cool more than useful. 
pub fn init_map(sensors: &[&Sensor]) {
    let mut stdout = stdout().into_raw_mode().unwrap(); // Necessary for printing
    write!(stdout, "{clear}",
           clear = clear::All).unwrap();

    // Prints min and max meters on left and right side of the distance-block map.
    for s in sensors.iter() {
        let left_text = String::from("0m |");
        let right_text = String::from(format!("| {}m", (s.max_distance as f32/1000.0)));

        write!(stdout, "{goto_l}{}{}{goto_r}{}",
               left_text,
               " ".repeat(s.gui.max_blocks as usize),
               right_text,
               goto_l = cursor::Goto(s.gui.x-left_text.len() as u16, s.gui.y),
               goto_r = cursor::Goto(s.gui.x+s.gui.max_blocks, s.gui.y)).unwrap();

        for step in 0..s.gui.max_blocks {
            write!(stdout, "{goto_blocks}{}",
                     "=",
                     goto_blocks = cursor::Goto(s.gui.x+step, s.gui.y)).unwrap();
            stdout.flush().unwrap();
            print_delay(2);
        }
    }
}

// Re-prints the 'distance-block' map to clean it up. Usually before moving the X.
pub fn print_map(s: &Sensor) { 
    let mut stdout = stdout().into_raw_mode().unwrap();
    
    write!(stdout, "{goto}{col}{}", 
             "=".repeat(s.gui.max_blocks as usize),
             col = color::Fg(color::Green),
             goto = cursor::Goto(s.gui.x,s.gui. y)).unwrap();
    stdout.flush();
}

// Prints an X onto the appropriate block representing the distance value.
pub fn print_x(s: &Sensor, pos: u16) {
    let mut stdout = stdout().into_raw_mode().unwrap();
    write!(stdout, "\r{goto}{col}x{reset}",
             goto = cursor::Goto(pos+s.gui.x-1, s.gui.y),
             col = color::Fg(color::Red),
             reset = color::Fg(color::Reset)).unwrap();  
}

// Prints the positions where the X has been to give a history.
pub fn print_x_history(s: &Sensor, pos: u16){
    let mut stdout = stdout().into_raw_mode().unwrap();
    write!(stdout, "\r{goto}{col}x{reset}",
             goto = cursor::Goto(s.gui.x+pos-1, s.gui.y+1),
             col = color::Fg(color::Yellow),
             reset = color::Fg(color::Reset)).unwrap(); 
}

// Prints the block_number above the X.
pub fn print_block_num(s: &Sensor, pos: u16) {
    let mut stdout = stdout().into_raw_mode().unwrap();
    write!(stdout, "{goto}{clear}{col}{}{reset}",
             pos,
             goto = cursor::Goto(s.gui.x+pos-1, s.gui.y-1),
             col = color::Fg(color::LightCyan),
             clear = clear::CurrentLine,
             reset = color::Fg(color::Reset)).unwrap(); 
}

// Prints the range data values in mm, cm, dm and m for the
// distance between sensor and closest object.
fn print_pos_distances(s: &Sensor, current_distance: u16, pos: u16) {
    let mut stdout = stdout().into_raw_mode().unwrap(); 

    let d = get_distance(current_distance as f32);
    let (mm, cm, dm, m) = (d.0, d.1, d.2,d.3);
 
    // Warns if distance is smaller than warning_distance.
    if (mm as u16) < s.warning_distance {
        write!(stdout, "{goto}{clear}{col}{}{reset}",
               "COLLISSION IMMINENT!", //all is not good
               col = color::Fg(color::Red),
               clear = clear::CurrentLine,
               reset = color::Fg(color::Reset),
               goto = cursor::Goto(s.gui.x, s.gui.y-4)).unwrap();
    }
    else {
        write!(stdout, "{goto}{clear}{col}{}{reset}",
               "YEAAAAAAH BOIIIIIII", //all is good
               clear = clear::CurrentLine,
               col = color::Fg(color::Green),
               reset = color::Fg(color::Reset),
               goto = cursor::Goto(s.gui.x, s.gui.y-4)).unwrap();
    }
    
    // Look at all of those cool formatters!
    write!(stdout, "{goto}{clear}{col}{}mm | {:.3}cm | {:.3}dm | {:.4}m{reset}",
           mm.to_string(),
           cm.to_string(),
           dm.to_string(),
           m.to_string(),
           clear = clear::CurrentLine,
           col = color::Fg(color::Yellow),
           reset = color::Fg(color::Reset),
           goto = cursor::Goto(s.gui.x, s.gui.y-3)).unwrap();
}

// Gets distances in mm, cm, dm and m, as a tuple of f32s no less!
fn get_distance(current_distance: f32) -> (f32, f32, f32, f32) {
    let mm = (current_distance as f32);
    let cm = (current_distance/10.0 as f32);
    let dm = (current_distance/100.0 as f32);
    let m  = (current_distance/1000.0 as f32);
    (mm, cm, dm, m)
}

// Just moves the cursor out of the CLI.
fn print_end_clear(s: &Sensor) {
    let mut stdout = stdout().into_raw_mode().unwrap(); 
    write!(stdout, "{goto}{clear}",
           clear = clear::CurrentLine,
           goto = cursor::Goto(1, s.gui.y+10)).unwrap();

}


// Some additional, more easy-to-read text on the distance in mm and what block is used.
// Also gives out warnings if a value outside of the max_distance/min_distance scope is given.
pub fn print_distance(s: &Sensor, current_distance: u16) {
    let mut stdout = stdout().into_raw_mode().unwrap();
    let mm_per_block: f32 = s.max_distance as f32 / s.gui.max_blocks as f32 ; //6500/65=100, that is: 100mm per block
    let block_pos: u16 = (0.5 + (current_distance as f32/ mm_per_block)) as u16;

    // If illegal distance values are recieved!!!
    if block_pos > s.gui.max_blocks || block_pos < s.gui.min_blocks{
        write!(stdout, "{goto}{col}Invalid Allowed Distance>>> Expected: 0-{}mm | Actual: {}mm{reset}{goto_panic}\n\n\n", 
               s.max_distance,
               current_distance,
               goto = cursor::Goto(s.gui.x, s.gui.y+3),
               goto_panic = cursor::Goto(0, s.gui.y+5),
               col = color::Fg(color::Red),
               reset = color::Fg(color::Reset)).unwrap();
        //panic!(format!("A value that is not deemed allowed was received: \
        //{}mm", current_distance));
        
        //Clears the GUI Position and Block Num
        clear_position_data(&s, current_distance as u16, block_pos);
    }
    else {
        write!(stdout, "{goto}{clear}Distance: {}mm, Block: {} ",
               current_distance, 
               block_pos,
               clear = clear::CurrentLine,
               goto = cursor::Goto(s.gui.x, s.gui.y+3)).unwrap();
    
        print_position(s, current_distance as u16, block_pos);
    } 
}

// Just some more error-printing code. In case of Invalid Range Data received
// Invalid Range Data is data that breaks either an upper or lower threshold (in mm)
fn clear_position_data(s: &Sensor, current_distance: u16, pos: u16) {
    let mut stdout = stdout().into_raw_mode().unwrap();
    print_map(&s);
    write!(stdout, "{goto}{clear}",
         goto = cursor::Goto(s.gui.x-1, s.gui.y-1),
         clear = clear::CurrentLine);
    write!(stdout, "{goto}{clear}{col}{}{reset}",
           "ERROR: INVALID RANGE DATA",
           col = color::Fg(color::Red),
           clear = clear::CurrentLine,
           reset = color::Fg(color::Reset),
           goto = cursor::Goto(s.gui.x, s.gui.y-4)).unwrap();
    write!(stdout, "{goto}{clear}",
           clear = clear::CurrentLine,
           goto = cursor::Goto(s.gui.x, s.gui.y-4)).unwrap();
    print_end_clear(&s); 
}

// Prints the raw received data. Good for debug and making sure it's indeed
// not your code that's broken, but your friend's code which is sending whacky stuff.
pub fn print_raw_data(s: &Sensor, buf: &[u8]) { 
    let mut stdout = stdout().into_raw_mode().unwrap();
    write!(stdout, "{goto}{clear}{col}Raw Data: {:?}{reset}",
           buf,
           col = color::Fg(color::Yellow),
           clear = clear::CurrentLine,
           reset = color::Fg(color::Reset),
           goto = cursor::Goto(s.gui.x, s.gui.y+8)).unwrap();
}

// Literally prints anything. Use if you're absolutely desperate for printing debug data 
// in a crude and messy fashion without wanting to re-write new functions.
pub fn print_anything(s: &Sensor, anything: String) {
    let mut stdout = stdout().into_raw_mode().unwrap();
    write!(stdout, "{goto}{clear}{col}Anyting: {}{reset}",
           anything,
           col = color::Fg(color::Yellow),
           clear = clear::CurrentLine,
           reset = color::Fg(color::Reset),
           goto = cursor::Goto(s.gui.x, s.gui.y+7)).unwrap();

}


// The infamous print delay. Helpful for debugging
// Use to see what's going on in the gui if things are going too quick. 
fn print_delay(delay: u64) {
    thread::sleep(Duration::from_millis(delay));
}

