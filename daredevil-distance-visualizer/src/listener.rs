use std::net::{IpAddr, Ipv4Addr};
use std::net::{TcpListener, TcpStream};
use std::io::prelude::*;
use std::io::{Write, stdout};

// Note what kind of import this is
// It's the identical imports to the ones in main.rs
// Any other import gets real fucky wuckey so just don't.
// Or rewrite everything in a convoluted mess of a mod structure. I won't stop you. 
// Suffering develops character.
use crate::sensor::Sensor;
use crate::SENSOR_AMOUNT;
use crate::gui;


// If you know nothing about Socket Programming with Rust, just go read Chapter 20
// in the Rust Book. Or was it 21? No, I'm certain it was 20. Whatever, it's one of 
// the later ones I assure you.

// Listener function. 
// This bad boy is what brings listening functionality to put the program.
// Listens to ip:port, (use localhost and arbitrary port for debug)
pub fn listener(s: &[&Sensor], ip: IpAddr, port: u16) {
    // Initiate the listener to an ip and a port.
    let listener = TcpListener::bind(format!("{}:{}", ip, port)).unwrap();

    // Forever listening for connections...
    loop {
        // Triggers once a connection is established
        for stream in listener.incoming() {
            let stream = stream.unwrap();
            
            // This handles the data and sends it over the the gui.rs functions
            // to print the data accordingly.
            get_and_print_message_data(s, &stream);
        }
    }
}

// Handles the data from the TCP stream received and sends it off for being
// printed onto the CLI.
pub fn get_and_print_message_data(s: &[&Sensor], mut stream: &TcpStream) { 
    let mut buffer: [u8; 8] = [0; 8]; //Should contain four u16 values
    
    loop {
        // Peeks to make sure there's data in the stream
        let mut peek = stream.peek(&mut buffer).expect("peek failed");
        //While there's at least 8 bytes of data in the stream (four u16), keep fetching data.
        while peek >= 8 {
            stream.read(&mut buffer).unwrap(); // Puts data into buffer
            //gui::print_raw_data(&(s[0]), &buffer);
           
            //Splits up the data in buffer into four u16s. These values are in mm.
            let v1: u16 = ((buffer[0] as u16) << 8) + ((buffer[1] as u16) << 0);
            let v2: u16 = ((buffer[2] as u16) << 8) + ((buffer[3] as u16) << 0);
            let v3: u16 = ((buffer[4] as u16) << 8) + ((buffer[5] as u16) << 0);
            let v4: u16 = ((buffer[6] as u16) << 8) + ((buffer[7] as u16) << 0);
            let sensor_values: [u16; 4] = [v1, v2, v3, v4];

            peek = stream.peek(&mut buffer).expect("peek failed");
            
            //Sends the values to the printing department.
            print_message(s, &sensor_values);
        }
    }

}

// Prints the distance data for all four sensors. Hard coded, bby.
pub fn print_message(sensors: &[&Sensor], sensor_values: &[u16;4]) {
    for i in 0..SENSOR_AMOUNT {
        gui::print_distance(sensors[i], sensor_values[i]);
    }
}
