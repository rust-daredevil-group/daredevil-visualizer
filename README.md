# Visualizer for the Daredevil Project

`daredevil-visualizer` is a collection of projects that help in visualizing data for the Daredevil Project. 
The data that is visualized is sensor data that comes from the sensor arrays attached to the light modules.

## Daredevil-Distance-Visualizer
The actual visualizer program. It uses [termion](https://gitlab.redox-os.org/redox-os/termion) to 
visualize one or more sensors in a CLI application. The data is visualized in the form of one
or several progress bars that represent distance from a base point to a maximum pre-determined
distance of the sensors.The program has the ability to render and type out the approximate distance of 
any closest object to each sensor.

Read the [README.md](https://gitlab.com/rust-daredevil-group/daredevil-visualizer/tree/master/daredevil-distance-visualizer/README.md)
in the Daredevil-Distance-Visualizer directory for more in depth info.

## Daredevil-Echo
Daredevil-Echo is a helper program written in Python which simply sends data over a socket, 
in any pre-defined way the programmer wants to. This is used to debug the Daredevil-Distance-Visualizer.

Read the [README.md](https://gitlab.com/rust-daredevil-group/daredevil-visualizer/tree/master/daredevil-echo/README.md)
in the Daredevil-Echo directory for more in depth info.
